// Fill out your copyright notice in the Description page of Project Settings.


#include "BTService_UpdatePlayerLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

#include "SkyrimCloneAIController.h"
#include "SkyrimCloneCharacter.h"

UBTService_UpdatePlayerLocation::UBTService_UpdatePlayerLocation()
{
    //Set name of node in behavior tree
	NodeName = TEXT("Update Player Location If Seen");
}

void UBTService_UpdatePlayerLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	//Get the AI controller's owning pawn, and cast to a SkyrimCloneCharacter
	AIOwner = Cast<ASkyrimCloneCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (AIOwner == nullptr) { return; }

	//Get the first (aka only) player character
	TargetCharacter = Cast<ASkyrimCloneCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (TargetCharacter == nullptr) { return; }

	//Find distance between AI controller's owner and its target using the distance formula
	DistanceVectorToTarget = AIOwner->GetActorLocation() - TargetCharacter->GetActorLocation();
	DistanceToTarget = sqrtf(pow(DistanceVectorToTarget.X, 2) + pow(DistanceVectorToTarget.Y, 2) + pow(DistanceVectorToTarget.Z, 2));

	//If target character is within AI character's vision range and is not in stealth mode, update player's location
	if ((DistanceToTarget < AIOwner->AIDetectionRange) && !TargetCharacter->bIsInStealth && AIOwner->bIsInCombat == 1)
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), TargetCharacter->GetActorLocation());
	}
	//If character is in stealth mode, half the attacker's effective vision range
	else if ((DistanceToTarget < AIOwner->AIDetectionRange / 2) && TargetCharacter->bIsInStealth && AIOwner->bIsInCombat == 1)
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), TargetCharacter->GetActorLocation());
	}
	//Otherwise clear player's current location
	else
	{
		AIOwner->bIsInCombat = 0;
		OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
	}
}