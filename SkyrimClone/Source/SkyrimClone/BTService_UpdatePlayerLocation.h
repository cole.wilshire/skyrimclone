// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"

#include "SkyrimCloneCharacter.h"

#include "BTService_UpdatePlayerLocation.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API UBTService_UpdatePlayerLocation : public UBTService_BlackboardBase
{
	GENERATED_BODY()
	
public:
	UBTService_UpdatePlayerLocation();

protected:
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

private:
	//AI controller's controlled character
	ASkyrimCloneCharacter* AIOwner = nullptr;

	//Character being targeted by AI Controller
	ASkyrimCloneCharacter* TargetCharacter = nullptr;

	//Distance between AI's owner and its target
	float DistanceToTarget;

	FVector DistanceVectorToTarget;
};
