// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_Attack.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

#include "SkyrimCloneAIController.h"
#include "SkyrimCloneCharacter.h"

UBTTask_Attack::UBTTask_Attack()
{
	//Set node name in behavior tree
	NodeName = TEXT("Attack");
}

EBTNodeResult::Type UBTTask_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	//Retun failure of behavior tree has no owning AI controller
	if (OwnerComp.GetAIOwner() == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	//Get character from behavior tree's associated AI controller
	ASkyrimCloneCharacter* Character = Cast<ASkyrimCloneCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (Character == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	//Get player pawn and set AI to focus on it. This will make the AI always face the player
	APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (PlayerPawn != nullptr)
	{
		OwnerComp.GetAIOwner()->SetFocus(PlayerPawn);
	}

	//Command character to attack
	Character->CurrentWeapon->Attack();

	//Node finished successfully
	return EBTNodeResult::Succeeded;
}