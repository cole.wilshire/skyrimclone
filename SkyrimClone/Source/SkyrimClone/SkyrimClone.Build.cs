// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SkyrimClone : ModuleRules
{
	public SkyrimClone(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG", "GameplayTasks" });
	}
}
