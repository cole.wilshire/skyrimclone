// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkyrimClone.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkyrimClone, "SkyrimClone" );
 