// Fill out your copyright notice in the Description page of Project Settings.


#include "SkyrimCloneAIController.h"

void ASkyrimCloneAIController::BeginPlay()
{
    Super::BeginPlay();

    //Set behavior tree to the one specified in character's Blueprint
    RunBehaviorTree(AIBehavior);
}

void ASkyrimCloneAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
