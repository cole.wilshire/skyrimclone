// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SkyrimCloneAIController.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API ASkyrimCloneAIController : public AAIController
{
	GENERATED_BODY()

private:
	//Called every frame
	virtual void Tick(float DeltaTime) override;

	//Called when character is spawned
	virtual void BeginPlay() override;

	//Set AI behavior tree to be used
	UPROPERTY(EditAnywhere)
	class UBehaviorTree* AIBehavior;
};
