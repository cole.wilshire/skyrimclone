// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkyrimCloneCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Math/Vector.h"

#include "SkyrimClonePlayerController.h"

#include "SkyrimCloneGameInstance.h"

//////////////////////////////////////////////////////////////////////////
// ASkyrimCloneCharacter

ASkyrimCloneCharacter::ASkyrimCloneCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->MaxWalkSpeed = MaximumWalkingSpeed;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

void ASkyrimCloneCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Set current stats to max stats
	CurrentHealth = MaxHealth;
	CurrentStamina = MaxStamina;
	CurrentMagicka = MaxMagicka;

	//Spawn character's default weapons
	SpawnWeapons();

	//Get game instance and cast to SkyrimCloneGameInstance
	USkyrimCloneGameInstance* GameInstance = Cast<USkyrimCloneGameInstance>(GetGameInstance());
	if (!ensure(GameInstance != nullptr)) return;

	//Show player the User Interface if not on Main Menu screen
	if (GetWorld()->GetMapName() != TEXT("MainMenu"))
	{
		GameInstance->ShowUI();
	}

	//Add spawned weapons to inventory
	Inventory.Add(CurrentWeapon);
	Inventory.Add(OffhandWeapon);
}

// Called every frame
void ASkyrimCloneCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Make sure stats fall no lower than 0
	ClampStats();
	
	//Handle stamina depletion and recharge
	HandleStamina();

	//Handle magicka recharge
	HandleMagicka();
}

//////////////////////////////////////////////////////////////////////////
// Input

void ASkyrimCloneCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ASkyrimCloneCharacter::Attack); // Bind heavy attack to holding left mouse click
	PlayerInputComponent->BindAction("QuickAttack", IE_Released, this, &ASkyrimCloneCharacter::QuickAttack); // Bind light attack to releasing left mouse click
	PlayerInputComponent->BindAction("UseOffhandWeapon", IE_Pressed, this, &ASkyrimCloneCharacter::UseOffhandWeapon); // Bind offhand weapon to right mouse click
	PlayerInputComponent->BindAction("BlockEnd", IE_Released, this, &ASkyrimCloneCharacter::BlockEnd); // Bind block to right mouse click
	PlayerInputComponent->BindAction("StartSprinting", IE_Pressed, this, &ASkyrimCloneCharacter::StartSprinting); // Bind sprinting to pressing shift
	PlayerInputComponent->BindAction("StopSprinting", IE_Released, this, &ASkyrimCloneCharacter::StopSprinting); // Bind stop sprinting to lifting off shift
	PlayerInputComponent->BindAction("OpenInGameMenu", IE_Released, this, &ASkyrimCloneCharacter::OpenInGameMenu); // Bind opening the in-game menu to escape and m
	PlayerInputComponent->BindAction("OpenInventoryMenu", IE_Released, this, &ASkyrimCloneCharacter::OpenInventoryMenu); // Bind opening the inventory menu to i and start
	PlayerInputComponent->BindAction("ToggleStealth", IE_Released, this, &ASkyrimCloneCharacter::ToggleStealth); // Bind opening the toggle stealth to left ctrl
	PlayerInputComponent->BindAction("Interact", IE_Released, this, &ASkyrimCloneCharacter::Interact); // Bind interacting to E key or A button

	PlayerInputComponent->BindAxis("MoveForward", this, &ASkyrimCloneCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASkyrimCloneCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASkyrimCloneCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASkyrimCloneCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ASkyrimCloneCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ASkyrimCloneCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ASkyrimCloneCharacter::OnResetVR);
}


void ASkyrimCloneCharacter::OnResetVR()
{
	// If SkyrimClone is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in SkyrimClone.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ASkyrimCloneCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ASkyrimCloneCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ASkyrimCloneCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASkyrimCloneCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASkyrimCloneCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ASkyrimCloneCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

//Prevents stats from falling below 0 or rising above their maximum values
void ASkyrimCloneCharacter::ClampStats()
{
	//Prevent stats from falling below 0
	if (CurrentHealth < 0)
	{
		CurrentHealth = 0;
	}

	if (CurrentStamina < 0)
	{
		CurrentStamina = 0;
	}

	if (CurrentMagicka < 0)
	{
		CurrentMagicka = 0;
	}

	//Prevent stats from rising above their max values
	if (CurrentHealth > MaxHealth)
	{
		CurrentHealth = MaxHealth;
	}

	if (CurrentStamina > MaxStamina)
	{
		CurrentStamina = MaxStamina;
	}

	if (CurrentMagicka > MaxMagicka)
	{
		CurrentMagicka = MaxMagicka;
	}
}

//Handle stamina depletion and recharge
void ASkyrimCloneCharacter::HandleStamina()
{
	//Deplete stamina while sprinting
	if (bIsSprinting)
	{
		--CurrentStamina;
	}
	//Recharge stamina when not sprinting
	else
	{
		++CurrentStamina;
	}

	//Stop sprinting and blocking if stamina hits 0
	if (CurrentStamina <= 0)
	{
		StopSprinting();
		OffhandWeapon->BlockEnd();
	}
}

//Handle magicka recharge
void ASkyrimCloneCharacter::HandleMagicka()
{
	++CurrentMagicka;
}

//Opens in-game menu
void ASkyrimCloneCharacter::OpenInGameMenu()
{
	USkyrimCloneGameInstance* GameInstance = Cast<USkyrimCloneGameInstance>(this->GetGameInstance());
	if (!ensure(GameInstance != nullptr)) return;
	GameInstance->InGameLoadMenu();
}

//Opens inventory menu
void ASkyrimCloneCharacter::OpenInventoryMenu()
{
	LoadInventoryMenu();
}

//Opens inventory menu
void ASkyrimCloneCharacter::LoadInventoryMenu(ASkyrimCloneCharacter* InventoryOwner, ASkyrimCloneCharacter* InventoryAccessor)
{
	USkyrimCloneGameInstance* GameInstance = Cast<USkyrimCloneGameInstance>(GetGameInstance());
	if (!ensure(GameInstance != nullptr)) return;

	//If an inventory owner is not specified, default to the calling character as the inventory owner
	if (InventoryOwner == nullptr) {
		GameInstance->LoadInventoryMenu(this, this);
	}
	else
	{
		GameInstance->LoadInventoryMenu(InventoryOwner, this);
	}
}

//Increases max walk speed
void ASkyrimCloneCharacter::StartSprinting()
{
	if (CurrentStamina > 0)
	{
		EndStealth();
		BlockEnd();
		bIsSprinting = 1;
		GetCharacterMovement()->MaxWalkSpeed = MaximumSprintingSpeed;
	}
}

//Return to regular max speed
void ASkyrimCloneCharacter::StopSprinting()
{
	bIsSprinting = 0;
	GetCharacterMovement()->MaxWalkSpeed = MaximumWalkingSpeed;
}

//Begin sneaking if not in sneak mode
void ASkyrimCloneCharacter::BeginStealth()
{
	if (bIsInStealth == 0)
	{
		bIsInStealth = 1;
		GetCharacterMovement()->MaxWalkSpeed = MaximumStealthSpeed;
	}
}

//Stop sneaking if in sneak mode
void ASkyrimCloneCharacter::EndStealth()
{
	if (bIsInStealth == 1)
	{
		bIsInStealth = 0;
		GetCharacterMovement()->MaxWalkSpeed = MaximumWalkingSpeed;
	}
}

//Toggle between sneak mode and regular mode
void ASkyrimCloneCharacter::ToggleStealth()
{
	if (bIsInStealth == 0)
	{
		BeginStealth();
	}
	else
	{
		EndStealth();
	}
}

//Spawn weapon and assign it to character
void ASkyrimCloneCharacter::SpawnWeapons()
{
	//Spawn main weapon
	CurrentWeapon = GetWorld()->SpawnActor<ASkyrimCloneWeapon>(DefaultWeaponClass);
	//Attach weapon mesh to socket
	CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponAttachmentBone);
	//Set spawning character as owner
	CurrentWeapon->SetOwner(this);

	if (DefaultOffhandWeaponClass != nullptr)
	{
		//Spawn offhand weapon
		OffhandWeapon = GetWorld()->SpawnActor<ASkyrimCloneWeapon>(DefaultOffhandWeaponClass);
		//Attach weapon mesh to socket
		if (OffhandWeapon->WeaponType == EWeaponType::WT_Shield)
		{
			OffhandWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, ShieldAttachmentBone);
		}
		else
		{
			OffhandWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SpellAttachmentBone);
		}
		//Set spawning character as owner
		OffhandWeapon->SetOwner(this);
	}
}

//Equip selected weapon from inventory
void ASkyrimCloneCharacter::EquipWeapon(ASkyrimCloneWeapon* Weapon)
{
	if (Weapon->WeaponType == EWeaponType::WT_Weapon)
	{
		//Detach current weapon from character model, and put it under the floor for storage
		CurrentWeapon->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
		CurrentWeapon->SetActorLocation(FVector(0,0,-1000));
		
		//Set new weapon to be equipped as CurrentWeapon
		CurrentWeapon = Weapon;
		Weapon->SetOwner(this);
		Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponAttachmentBone);
	}
	else
	{
		//Detach offhand weapon from character model, and put it under the floor for storage
		OffhandWeapon->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
		OffhandWeapon->SetActorLocation(FVector(0, 0, -1000));

		//Set new weapon to be equipped as OffhandWeapon
		OffhandWeapon = Weapon;
		Weapon->SetOwner(this);
		if (Weapon->WeaponType == EWeaponType::WT_Shield)
		{
			Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, ShieldAttachmentBone);
		}
		else
		{
			Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SpellAttachmentBone);
		}
	}

	//Cancel weapon states, to prevent bugs when switching mid-attack
	CurrentWeapon->AttackEnd();
	OffhandWeapon->AttackEnd();
	OffhandWeapon->BlockEnd();
}

//Take weapon from another character's inventory
void ASkyrimCloneCharacter::TakeWeapon(ASkyrimCloneWeapon* Weapon, ASkyrimCloneCharacter* WeaponOwner)
{
	//If weapon is a mainhand weapon
	if (Weapon->WeaponType == EWeaponType::WT_Weapon)
	{
		//Detach current weapon from character model, and put it under the floor for storage
		WeaponOwner->CurrentWeapon->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
		WeaponOwner->CurrentWeapon->SetActorLocation(FVector(0, 0, -1000));

		//Clear owner's pointer to their former weapon
		WeaponOwner->CurrentWeapon = nullptr;
	}
	//If weapon is an offhand weapon
	else
	{
		//Detach offhand weapon from character model, and put it under the floor for storage
		WeaponOwner->OffhandWeapon->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
		WeaponOwner->OffhandWeapon->SetActorLocation(FVector(0, 0, -1000));

		//Clear owner's pointer to their former weapon
		WeaponOwner->OffhandWeapon = nullptr;
	}

	//Add pointer to weapon to weapon taker's inventory
	Inventory.Add(Weapon);
}

//Make weapon attack
void ASkyrimCloneCharacter::Attack()
{
	if (!OffhandWeapon->IsAttacking())
	{
		//CurrentWeapon->Attack();
		CurrentWeapon->bIsChargingHeavyAttack = 1;
	}
}

//Interrupt heavy attack timer and deal a light attack instead
void ASkyrimCloneCharacter::QuickAttack()
{
	if (!CurrentWeapon->IsAttacking() && !OffhandWeapon->IsAttacking())
	{
		CurrentWeapon->QuickAttack();
	}
}

//Make offhand weapon block or fire spell
void ASkyrimCloneCharacter::UseOffhandWeapon()
{
	//If using shield, block
	if (OffhandWeapon->WeaponType == EWeaponType::WT_Shield)
	{
		if (!CurrentWeapon->IsAttacking() && CurrentStamina != 50)
		{
			OffhandWeapon->Block();
			StopSprinting();
			GetCharacterMovement()->MaxWalkSpeed = MaximumStealthSpeed;
		}
	}
	//If using spell, fire spell
	else if (OffhandWeapon->WeaponType == EWeaponType::WT_Spell)
	{
		if (!CurrentWeapon->IsAttacking() && CurrentMagicka != 50)
		{
			OffhandWeapon->CastSpell();
		}
	}
}

//Make character stop blocking
void ASkyrimCloneCharacter::BlockEnd()
{
	OffhandWeapon->BlockEnd();
	GetCharacterMovement()->MaxWalkSpeed = MaximumWalkingSpeed;
}

float ASkyrimCloneCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);	//Call parent implementation of method first, as to not completely override its functionality
	//DamageToApply = FMath::Min(CurrentHealth, DamageToApply);	//Do not allow health to fall below 0

	//Reduce damage if blocking, but take that damage out of stamina instead
	if (OffhandWeapon->WeaponState == EWeaponState::WS_Blocking)
	{
		CurrentHealth -= DamageToApply / 2;
		CurrentStamina -= DamageToApply / 2;
	}
	else
	{
		CurrentHealth -= DamageToApply;
	}

	//Alert character if they were not alert before
	bIsInCombat = 1;

	return DamageToApply;
}

//Return whether or not a character is dead
bool ASkyrimCloneCharacter::IsDead() const
{
	return CurrentHealth <= 0;
}

//Knockback handling
void ASkyrimCloneCharacter::Knockback()
{
	//Return weapon state to default, so we don't end up with an infinite sword glitch if attack is interrupted
	CurrentWeapon->AttackEnd();

	//Get game instance and cast to SkyrimCloneGameInstance
	USkyrimCloneGameInstance* GameInstance = Cast<USkyrimCloneGameInstance>(GetGameInstance());
	if (!ensure(GameInstance != nullptr)) return;

	//Get first (aka only) player controller
	ASkyrimClonePlayerController* PlayerController = Cast<ASkyrimClonePlayerController>(GameInstance->GetFirstLocalPlayerController());
	if (!ensure(PlayerController != nullptr)) return;

	//Get character from player controller
	ASkyrimCloneCharacter* PlayerCharacter = Cast<ASkyrimCloneCharacter>(PlayerController->GetViewTarget());
	if (!ensure(PlayerCharacter != nullptr)) return;

	//Determine whether or not player character is in front of or behind character using the dot product of their location vectors
	//float DotProductOfLocations = (PlayerCharacter->GetActorForwardVector() - GetActorForwardVector()).Normalize();
	//if (DotProductOfLocations >= 0)

	if(GetController()->LineOfSightTo(PlayerController))
	{
		PlayAnimMontage(FrontHitMontage, 1.0f, "Default");
	}
	else
	{
		PlayAnimMontage(BackHitMontage, 1.0f, "Default");
	}
}

//Interact with an interactable object
void ASkyrimCloneCharacter::Interact()
{
	if (LastViewedActor != nullptr)
	{
		ASkyrimCloneCharacter* LastViewedCharacter = Cast<ASkyrimCloneCharacter>(LastViewedActor);
		if (!ensure(LastViewedCharacter != nullptr)) return;

		//If facing a corpse, enable loot interaction
		if(LastViewedCharacter->IsDead())
		{
			LootCharacter();
		}
	}
}

//Open a dead character's inventory
void ASkyrimCloneCharacter::LootCharacter()
{
	ASkyrimCloneCharacter* CharacterToLoot = Cast<ASkyrimCloneCharacter>(LastViewedActor);
	if (!ensure(CharacterToLoot != nullptr)) return;

	LoadInventoryMenu(CharacterToLoot);
}