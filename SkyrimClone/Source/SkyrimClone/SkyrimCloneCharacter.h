// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "SkyrimCloneWeapon.h"

#include "SkyrimCloneCharacter.generated.h"

//Character's relationship to other characters, for friendly fire
UENUM(BlueprintType)
enum class ECharacterFaction : uint8
{
	Faction_Ally,
	Faction_Neutral,
	Faction_Enemy,
	Faction_Dead,
	Faction_None
};

UCLASS(config=Game)
class ASkyrimCloneCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ASkyrimCloneCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	//Character's external name
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Player)
	FName Name;

	//Store's character's currently equipped weapon object
	UPROPERTY(BlueprintReadWrite, Category = Player)
	ASkyrimCloneWeapon* CurrentWeapon;

	//Store's character's currently equipped offhand weapon
	UPROPERTY(BlueprintReadWrite, Category = Player)
	ASkyrimCloneWeapon* OffhandWeapon;

	//Store's character's currently equipped offhand weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Player)
	ECharacterFaction Faction;

	//Spawn character's weapons
	void SpawnWeapons();

	//Equip new set of weapons
	void EquipWeapon(ASkyrimCloneWeapon* Weapon);

	//Take weapon from another character's inventory
	void TakeWeapon(ASkyrimCloneWeapon* Weapon, ASkyrimCloneCharacter* WeaponOwner);

	//Determine's character's default weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	TSubclassOf<ASkyrimCloneWeapon> DefaultWeaponClass;

	//Determine's character's default offhand weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	TSubclassOf<ASkyrimCloneWeapon> DefaultOffhandWeaponClass;

	//Attack using current weapon
	void Attack();

	//Perform a light attack using current weapon
	void QuickAttack();

	//Block or attack with spell using offhand weapon
	void UseOffhandWeapon();

	//Stop blocking with offhand weapon
	void BlockEnd();

	//Max health
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Statistics")
	float MaxHealth;

	//Max stamina
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Statistics")
	float MaxStamina;

	//Max magicka
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Statistics")
	float MaxMagicka;

	//Current health
	UPROPERTY(BlueprintReadWrite)
	float CurrentHealth;

	//Current stamina
	UPROPERTY(BlueprintReadWrite)
	float CurrentStamina;

	//Current magicka
	UPROPERTY(BlueprintReadWrite)
	float CurrentMagicka;

	//Max non-sprint speed
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Statistics")
	float MaximumWalkingSpeed = 500;

	//Max sprint speed
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Statistics")
	float MaximumSprintingSpeed;

	//Max stealth speed
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Statistics")
	float MaximumStealthSpeed;

	//AI character's maximum detection range, unmodified by stealth maluses
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Statistics")
	float AIDetectionRange = 1000;

	//Damage-handling
	UFUNCTION(BlueprintCallable, Category = "Health")
	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	//Tells whether or not a character is dead. A pure blueprint node has no execution pin
	UFUNCTION(BlueprintPure)
	bool IsDead() const;

	//Knockback handling
	UFUNCTION(BlueprintCallable)
	void Knockback();

	//Tells whether or not a character is sprinting
	bool bIsSprinting = 0;

	//Tells whether or not a character is stealthed
	UPROPERTY(BlueprintReadWrite)
	bool bIsInStealth = 0;

	//Tells whether or not an AI character is in its combat mode
	UPROPERTY(BlueprintReadWrite)
	bool bIsInCombat = 0;

	//Holds a pointer to the last interactable actor a character looked at, to display their name on the UI
	UPROPERTY(BlueprintReadWrite)
	AActor* LastViewedActor = nullptr;

	//Montage that is played when characters are hit by standard attacks from the front
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* FrontHitMontage;

	//Montage that is played when characters are hit by standard attacks from the back
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* BackHitMontage;

	//Montage that is played when characters are hit by heavy attacks from the back
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* FrontKnockbackMontage;

	//Montage that is played when characters are hit by heavy attacks from the back
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* BackKnockbackMontage;

	//Bones to attach weapons to
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	FName WeaponAttachmentBone = TEXT("weapon_r");
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	FName ShieldAttachmentBone = TEXT("shield_l");
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	FName SpellAttachmentBone = TEXT("middle_01_l");

	//Inventory system consisting of a TArray of AWeapon pointers
	UPROPERTY(BlueprintReadWrite)
	TArray<ASkyrimCloneWeapon*> Inventory;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	//Open in-game menu
	void OpenInGameMenu();

	//Interface for load inventory menu, so it can be used with keybinding
	void OpenInventoryMenu();

	//Open inventory menu of specified character
	void LoadInventoryMenu(ASkyrimCloneCharacter* InventoryOwner = nullptr, ASkyrimCloneCharacter* InventoryAccessor = nullptr);

	//Increase max walk speed
	void StartSprinting();

	//Set max walk speed to default
	void StopSprinting();

	//Put character into stealth state
	void BeginStealth();

	//Remove character from stealth
	void EndStealth();

	//Toggle character's stealth state
	void ToggleStealth();

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

private:
	//Called every frame
	virtual void Tick(float DeltaTime) override;

	//Prevents stats from falling below 0 or rising above maximum
	void ClampStats();

	//Handle stamina depletion and recharge
	void HandleStamina();

	//Handle magicka recharge
	void HandleMagicka();

	//Handles the various types of interactions a player can make, such as looting and opening doors
	void Interact();

	//Loot a corpse character
	void LootCharacter();
};
