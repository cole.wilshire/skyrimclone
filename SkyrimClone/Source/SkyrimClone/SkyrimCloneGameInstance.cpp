// Fill out your copyright notice in the Description page of Project Settings.


#include "SkyrimCloneGameInstance.h"

#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"

#include "UserInterface/MainMenu.h"
#include "UserInterface/MenuWidget.h"
#include "UserInterface/UIWidget.h"
#include "UserInterface/InventoryMenu.h"

//Constructor
USkyrimCloneGameInstance::USkyrimCloneGameInstance(const FObjectInitializer& ObjectInitializer)
{
	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/SkyrimClone/UserInterface/WBP_MainMenu"));
	if (!ensure(MenuBPClass.Class != nullptr)) return;
	MenuClass = MenuBPClass.Class;

	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> InGameMenuBPClass(TEXT("/Game/SkyrimClone/UserInterface/WBP_InGameMenu"));
	if (!ensure(InGameMenuBPClass.Class != nullptr)) return;
	InGameMenuClass = InGameMenuBPClass.Class;

	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> InventoryMenuBPClass(TEXT("/Game/SkyrimClone/UserInterface/WBP_InventoryMenu"));
	if (!ensure(InventoryMenuBPClass.Class != nullptr)) return;
	InventoryMenuClass = InventoryMenuBPClass.Class;

	//Used for finding a Blueprint class for reference. Must be placed in the constructor.
	ConstructorHelpers::FClassFinder<UUserWidget> UIBPClass(TEXT("/Game/SkyrimClone/UserInterface/WBP_MainUI"));
	if (!ensure(UIBPClass.Class != nullptr)) return;
	UIClass = UIBPClass.Class;
}

void USkyrimCloneGameInstance::LoadMenuWidget()
{
	//Get menu class from Blueprint
	if (!ensure(MenuClass != nullptr)) return;	//Protection from pointer being null causing editor crash

	Menu = CreateWidget<UMainMenu>(this, MenuClass);
	if (!ensure(Menu != nullptr)) return;	//Protection from pointer being null causing editor crash

	//Set up main menu
	Menu->Setup();
}

void USkyrimCloneGameInstance::LoadMainMenu()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;	//Protection from player controller pointer being null causing crash

	PlayerController->ClientTravel("/Game/SkyrimClone/Maps/MainMenu", ETravelType::TRAVEL_Absolute);
}

void USkyrimCloneGameInstance::InGameLoadMenu()
{
	//Get menu class from Blueprint
	if (!ensure(InGameMenuClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UMenuWidget* InGameMenu = CreateWidget<UMenuWidget>(this, InGameMenuClass);
	if (!ensure(InGameMenu != nullptr)) return;	//Protection from pointer being null causing editor crash

	//Set up in-game menu
	InGameMenu->Setup();
}

void USkyrimCloneGameInstance::LoadInventoryMenu(ASkyrimCloneCharacter* InventoryOwner, ASkyrimCloneCharacter* InventoryAccessor)
{
	//Get menu class from Blueprint
	if (!ensure(InventoryMenuClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UInventoryMenu* InventoryMenu = CreateWidget<UInventoryMenu>(this, InventoryMenuClass);
	if (!ensure(InventoryMenu != nullptr)) return;	//Protection from pointer being null causing editor crash

	InventoryMenu->Setup(InventoryOwner, InventoryAccessor);
}

void USkyrimCloneGameInstance::ShowUI()
{
	//Get menu class from Blueprint
	if (!ensure(UIClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UUIWidget* UI = CreateWidget<UUIWidget>(this, UIClass);

	//Create UI
	UI->Setup();
}

void USkyrimCloneGameInstance::DestroyUI()
{
	//Get menu class from Blueprint
	if (!ensure(UIClass != nullptr)) return;	//Protection from pointer being null causing editor crash
	UUIWidget* UI = CreateWidget<UUIWidget>(this, UIClass);

	//Tear down UI
	UI->Teardown();
}