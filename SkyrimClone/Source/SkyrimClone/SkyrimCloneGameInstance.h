// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "UserInterface/MenuInterface.h"

#include "SkyrimCloneGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API USkyrimCloneGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	USkyrimCloneGameInstance(const FObjectInitializer& ObjectInitializer);

	//Functions callable in a Blueprint
	UFUNCTION(BlueprintCallable)
	void LoadMenuWidget();

	UFUNCTION(BlueprintCallable)
	void InGameLoadMenu();

	UFUNCTION(BlueprintCallable)
	void LoadInventoryMenu(ASkyrimCloneCharacter* InventoryOwner, ASkyrimCloneCharacter* InventoryAccessor);

	UFUNCTION(BlueprintCallable)
	void ShowUI();

	UFUNCTION(BlueprintCallable)
	void DestroyUI();

	void LoadMainMenu();

private:
	TSubclassOf<class UUserWidget> MenuClass;	//UUserWidget requires adding (UMG Unreal Motion Graphics) in FantasyBS.Build.cs to PublicDependencyModuleNames. See Section 2, video 3 (video 26) for help.
	TSubclassOf<class UUserWidget> InGameMenuClass;
	TSubclassOf<class UUserWidget> InventoryMenuClass;
	TSubclassOf<class UUserWidget> UIClass;

	class UMainMenu* Menu;
};
