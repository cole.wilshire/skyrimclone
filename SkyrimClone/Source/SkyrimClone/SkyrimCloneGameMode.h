// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SkyrimCloneGameMode.generated.h"

UCLASS(minimalapi)
class ASkyrimCloneGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASkyrimCloneGameMode();
};



