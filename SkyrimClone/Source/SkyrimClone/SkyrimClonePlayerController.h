// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SkyrimClonePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API ASkyrimClonePlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
