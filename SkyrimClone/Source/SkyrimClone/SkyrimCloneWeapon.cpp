// Fill out your copyright notice in the Description page of Project Settings.


#include "SkyrimCloneWeapon.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

#include "SkyrimCloneCharacter.h"

//Draw debug hit detection lines
static int32 DebugWeaponDrawing = 1;
FAutoConsoleVariableRef CVARDebugWeaponDrawing(
	TEXT("MELEE.DebugWeapons"),
	DebugWeaponDrawing,
	TEXT("Draw Debug Lines for Weapons"),
	ECVF_Cheat);

// Sets default values
ASkyrimCloneWeapon::ASkyrimCloneWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Setup root component
	WeaponRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(WeaponRoot);

	//Setup mesh component
	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	WeaponMesh->SetupAttachment(WeaponRoot);

	//Handle overlap events for weapon's mesh
	WeaponMesh->OnComponentBeginOverlap.AddDynamic(this, &ASkyrimCloneWeapon::OnBeginOverlap);

	//Set default weapon state of none
	SetWeaponState(EWeaponState::WS_None);
}

// Called when the game starts or when spawned
void ASkyrimCloneWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASkyrimCloneWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Handle timing for heavy attack
	HandleHeavyAttack();

	//Preliminary data setup
	FHitResult HitData(ForceInit);
	FHitResult MeshHitData(ForceInit);

	//Get weapon owner
	APawn* MyOwner = Cast<APawn>(GetOwner());
	ASkyrimCloneCharacter* Char = Cast<ASkyrimCloneCharacter>(MyOwner);

	//Ignore weapon's owner when tracing
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(MyOwner);

	//Disable complex trace
	Params.bTraceComplex = false;

	//Initialize damage to deal
	float DamageToDeal = 0.0f;

	//Ignore weapon's owner when tracing
	FCollisionQueryParams MeshParams;
	MeshParams.AddIgnoredActor(MyOwner);

	//Update the start and end socket locations
	StartSockEnd = WeaponMesh->GetSocketLocation(SocketStartName);
	EndSockEnd = WeaponMesh->GetSocketLocation(SocketEndName);
	
	//If tracing is enabled
	if (bPerformTracing)
	{
		//Get the start and end location to make the traces from
		StartSockEnd = WeaponMesh->GetSocketLocation(SocketStartName);
		EndSockEnd = WeaponMesh->GetSocketLocation(SocketEndName);

		//Adjust the damage we will deal based on the weapon state
		if (WeaponState == EWeaponState::WS_Attacking)
		{
			DamageToDeal = LightAttackDamage;
			
		}
		if (WeaponState == EWeaponState::WS_HeavyAttacking)
		{
			DamageToDeal = HeavyAttackDamage;
		}

		//Multiply damage by 2 if character is in stealth
		ASkyrimCloneCharacter* WeaponOwner = Cast<ASkyrimCloneCharacter>(GetOwner());
		if (WeaponOwner != nullptr)
		{
			if (WeaponOwner->bIsInStealth == 1)
			{
				DamageToDeal *= 2;
			}
		}

		//Make a number of trace lines, based on the specified traces per attack
		for (int i = 0; i < TracesPerAttack; i++)
		{
			//Create start and endpoint for each trace
			TraceStart = FMath::Lerp(StartSockEnd, EndSockEnd, i / TracesPerAttack);
			TraceEnd = FMath::Lerp(LastFrameStartSockEnd, LastFrameEndSockEnd, i / TracesPerAttack);

			//Draw debug lines for hit trace
			if (DebugWeaponDrawing > 0)
			{
				DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor(255, 0, 0), false, 1, 0, 0.5);
			}

			//First, trace use a standard trace using our vectors
			if (GetWorld()->LineTraceSingleByChannel(HitData, TraceStart, TraceEnd, ECollisionChannel::ECC_Pawn, Params))
			{
				//Get character hit by attack
				ASkyrimCloneCharacter* HitCharacter = Cast<ASkyrimCloneCharacter>(HitData.GetActor());	//This returning nullptr is what is causing the crash

				//OnHit blueprint event
				OnHit(HitData);

				if (HitCharacter != nullptr)
				{
					ASkyrimCloneCharacter* HitSkyrimCloneCharacter = Cast<ASkyrimCloneCharacter>(HitCharacter);
					ASkyrimCloneCharacter* OwnerCharacter = Cast<ASkyrimCloneCharacter>(GetOwner());
				
					//Do a component level trace, to get the more accurate hit location
					if (HitSkyrimCloneCharacter->GetMesh()->LineTraceComponent(MeshHitData, TraceStart, TraceEnd, MeshParams))
					{
						//Grab the pawn we have hit from the trace
						if (HitCharacter == MeshHitData.GetActor())
						{
							//Check if this pawn was hit previously on this swing ( if not then continue )
							if (CheckHurtCharacters(HitSkyrimCloneCharacter) == true)
							{
								//Get hit actor
								AActor* MeshHitActor = MeshHitData.GetActor();

								//If character is of a different faction
								if (HitSkyrimCloneCharacter->Faction != WeaponOwner->Faction)
								{
									//Damage character
									DealDamage(MyOwner, MeshHitActor, DamageToDeal, MeshHitData);

									//Play sound
									UGameplayStatics::PlaySound2D(GetWorld(), ImpactFleshSound);

									//Deal knockback to character if the attack was a heavy attack
									if (WeaponState == EWeaponState::WS_HeavyAttacking)
									{
										HitSkyrimCloneCharacter->Knockback();
									}

									//Play the impact effects on the exact point our trace hit
									PlayImpactEffects(EFXTypes::FXT_HitFlesh, MeshHitData.ImpactPoint);

									//Deal damage
									//UGameplayStatics::ApplyPointDamage(MeshHitActor, DamageToDeal, FVector(0, 0, 0), MeshHitData, MyOwner->GetInstigatorController(), MyOwner, DamageType);
									//Char->MulticastPlayReplicatedSound(false, ImpactFleshSound);

									//Draw debug sphere on exact contact location
									if (DebugWeaponDrawing > 0)
									{
										DrawDebugSphere(GetWorld(), MeshHitData.ImpactPoint, 12, 8, FColor::Green, false, 5.0f);
									}

									//Set impact point
									HitTraceInfo.HitLocation = MeshHitData.ImpactPoint;
									//HitTraceInfo.FXToPlay = EFXTypes::FXT_HitFlesh;
								}
							}
						}
					}
				}
			}
		}
		
		//Update the last frame vectors
		LastFrameStartSockEnd = StartSockEnd;
		LastFrameEndSockEnd = EndSockEnd;
	}
}

//Called when attack begins
void ASkyrimCloneWeapon::Attack()
{
	//Get weapon owner and cast to SkyrimCloneCharacter, checking for nullptrs
	ASkyrimCloneCharacter* WeaponOwner = Cast<ASkyrimCloneCharacter>(GetOwner());
	if (!ensure(WeaponOwner != nullptr)) return;

	//If another attack is not currently ongoing
	if (CanAttack())
	{
		//If we come here from not attacking
		if (WeaponState == EWeaponState::WS_None)
		{
			if(bIsChargingHeavyAttack)
			{
				//Set weapon state to attacking
				SetWeaponState(EWeaponState::WS_HeavyAttacking);
			}
			else
			{
				//Set weapon state to attacking
				SetWeaponState(EWeaponState::WS_Attacking);
			}
		}

		switch (WeaponState)
		{
		case (EWeaponState::WS_Attacking):
			//Disable tracing
			bPerformTracing = false;

			//Play WeaponSwingMontage, specified in weapon's blueprint
			if (!bUseAlternateAttack)
			{
				WeaponOwner->PlayAnimMontage(LightAttackMontage, 1.0f, "Default");
			}
			else if (bUseAlternateAttack)
			{
				WeaponOwner->PlayAnimMontage(AlternateAttackMontage, 1.0f, "Default");
			}

			//Play sound
			UGameplayStatics::PlaySound2D(GetWorld(), SwordSwingSound);

			//Emppty hit lists
			CharactersHitOnThisSwing.Empty();

			//Set ourself as currently swinging
			bCurrentlyAttacking = true;
			break;

		case (EWeaponState::WS_HeavyAttacking):
			//Charge player stamina for heavy attacking
			WeaponOwner->CurrentStamina -= HeavyAttackDamage;

			//Disable tracing
			bPerformTracing = false;

			//Play WeaponHeavyAttackMontage, specified in weapon's blueprint
			WeaponOwner->PlayAnimMontage(HeavyAttackMontage, 1.0f, "Default");

			//Play sound
			UGameplayStatics::PlaySound2D(GetWorld(), SwordSwingSound);

			//Emppty hit lists
			CharactersHitOnThisSwing.Empty();

			//Set ourself as currently swinging
			bCurrentlyAttacking = true;
			break;

		case EWeaponState::WS_None:
			break;
		}
	}
}

//Called when a heavy attack stops charging early
void ASkyrimCloneWeapon::QuickAttack()
{
	if(bIsChargingHeavyAttack == 1)
	{
		//Reset heavy attack variables before attacking. The order of operations is important to make sure a light attack occurs 
		bIsChargingHeavyAttack = 0;
		HeavyAttackTimer = 0;
		Attack();
	}
}

//Called when attack ends
void ASkyrimCloneWeapon::AttackEnd()
{
	//Reset weapon state to none
	SetWeaponState(EWeaponState::WS_None);

	//Stop trace
	bPerformTracing = false;

	//Stop attacking
	bCurrentlyAttacking = false;
	
	//Clear characters hit on this swing list
	CharactersHitOnThisSwing.Empty();
}

//Called when blocking begins
void ASkyrimCloneWeapon::Block()
{
	//Get weapon owner and cast to SkyrimCloneCharacter, checking for nullptrs
	ASkyrimCloneCharacter* WeaponOwner = Cast<ASkyrimCloneCharacter>(GetOwner());
	if (!ensure(WeaponOwner != nullptr)) return;

	//If another attack is not currently ongoing
	if (CanAttack())
	{
		//If we come here from not swinging
		if (WeaponState == EWeaponState::WS_None)
		{
			//Set weapon state to blocking
			SetWeaponState(EWeaponState::WS_Blocking);
		}

		switch (WeaponState)
		{
		case (EWeaponState::WS_Blocking):
			//Disable tracing
			bPerformTracing = false;

			//Play WeaponBlockMontage, specified in weapon's blueprint
			WeaponOwner->PlayAnimMontage(WeaponBlockMontage, 1.0f, "Default");

			//Emppty hit lists
			CharactersHitOnThisSwing.Empty();

			//Set ourself as currently swinging
			bCurrentlyAttacking = true;
			break;

		case EWeaponState::WS_None:
			break;
		}
	}
}

//Called when block ends
void ASkyrimCloneWeapon::BlockEnd()
{
	if (WeaponType == EWeaponType::WT_Shield)
	{
		//Reset weapon state to none
		SetWeaponState(EWeaponState::WS_None);

		//Get weapon owner and cast to SkyrimCloneCharacter, checking for nullptrs
		ASkyrimCloneCharacter* WeaponOwner = Cast<ASkyrimCloneCharacter>(GetOwner());
		if (!ensure(WeaponOwner != nullptr)) return;

		//Stop playing infinite loop of block montage
		WeaponOwner->StopAnimMontage(WeaponBlockMontage);

		//Stop trace
		bPerformTracing = false;

		//Stop attacking
		bCurrentlyAttacking = false;

		//Clear characters hit on this swing list
		CharactersHitOnThisSwing.Empty();
	}
}

//Play magic casting animation. Most functionality for spellcasting is contained in animation blueprint, due to heavy use of Blueprints and AnimNotifies
void ASkyrimCloneWeapon::CastSpell()
{
	//Get weapon owner and cast to SkyrimCloneCharacter, checking for nullptrs
	ASkyrimCloneCharacter* WeaponOwner = Cast<ASkyrimCloneCharacter>(GetOwner());
	if (!ensure(WeaponOwner != nullptr)) return;

	//If another attack is not currently ongoing
	if (CanAttack())
	{
		//If we come here from not attacking
		if (WeaponState == EWeaponState::WS_None)
		{
			//Set weapon state to attacking
			SetWeaponState(EWeaponState::WS_Attacking);
		}

		switch (WeaponState)
		{
		case (EWeaponState::WS_Attacking):
			//Charge player magicka for casting spell
			WeaponOwner->CurrentMagicka -= HeavyAttackDamage;

			//Disable tracing
			bPerformTracing = false;

			//Play LightAttackMontage, specified in weapon's blueprint
			WeaponOwner->PlayAnimMontage(LightAttackMontage, 1.0f, "Default");

			//Emppty hit lists
			CharactersHitOnThisSwing.Empty();

			//Set ourself as currently attacking
			bCurrentlyAttacking = true;
			break;

		case EWeaponState::WS_None:
			break;
		}
	}
}

//Return true if character is mid-attack
bool ASkyrimCloneWeapon::IsAttacking()
{
	if (WeaponState == EWeaponState::WS_Attacking || WeaponState == EWeaponState::WS_HeavyAttacking || WeaponState == EWeaponState::WS_Blocking)
	{
		return true;
	}

	return false;
}

//Deal damage to an actor
void ASkyrimCloneWeapon::DealDamage(APawn* DamageInstigator, AActor* ActorToDmg, float Dmg, FHitResult HitInfo)
{
	FHitResult Null;
	ASkyrimCloneCharacter* Char = Cast<ASkyrimCloneCharacter>(DamageInstigator);

	//Apply damage to the actor we have hit
	UGameplayStatics::ApplyPointDamage(ActorToDmg, Dmg, HitInfo.Location, HitInfo, DamageInstigator->GetInstigatorController(), DamageInstigator, DamageType);
	
	//Set hit location
	HitTraceInfo.HitLocation = HitInfo.ImpactPoint;

	//Set last hit actor
	LastHitActor = ActorToDmg;
}

//Create particle effects on hit location
void ASkyrimCloneWeapon::PlayImpactEffects(EFXTypes FXToPlay, FVector ImpactLoc)
{
	//Play the appropiate particle system based on the EFXTypes given
	switch (FXToPlay)
	{

	case EFXTypes::FXT_HitFlesh:
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactFlesh, ImpactLoc, ImpactLoc.Rotation(), true);
		break;
	}

	case EFXTypes::FXT_HitWorld:
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactWorldFX, ImpactLoc, ImpactLoc.Rotation(), true);
		break;
	}

	case EFXTypes::FXT_HitSword:
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactSwordFX, ImpactLoc, ImpactLoc.Rotation(), true);
		break;
	}

	case EFXTypes::FXT_HitAbsorb:
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactSwordFX, ImpactLoc, ImpactLoc.Rotation(), true);
		break;
	}

	}
}

//If character has yet to be injrued by attack, add it to list of the injured
bool ASkyrimCloneWeapon::CheckHurtCharacters(ACharacter* HurtChar)
{
	int i = CharactersHitOnThisSwing.Find(HurtChar);

	if (i != INDEX_NONE)
	{
		return false;
	}
	else
	{
		CharactersHitOnThisSwing.Add(HurtChar);
	}

	return true;
}

//Change the weapon state of weapon
void ASkyrimCloneWeapon::SetWeaponState(EWeaponState NewWeaponState)
{
	EWeaponState OldWeaponState = WeaponState;
	WeaponState = NewWeaponState;

	//If our state is none, then stop tracing
	if (NewWeaponState == EWeaponState::WS_None)
	{
		bPerformTracing = false;
	}
}

//Whether or not a weapon can be used to attack
bool ASkyrimCloneWeapon::CanAttack()
{
	//If we are not swinging already
	if (bCurrentlyAttacking == false)
	{
		return true;
	}

	else {
		return false;
	}
}

//Toggle tracing on the weapon
void ASkyrimCloneWeapon::ToggleTracing()
{
	//If currently tracing, stop tracing
	if (bPerformTracing == true)
	{
		bPerformTracing = false;
		return;

	}

	//If currently not tracing, start tracing
	if (bPerformTracing == false)
	{
		bPerformTracing = true;
		return;
	}
}

//Toggle tracing on the weapon
void ASkyrimCloneWeapon::ToggleAttackType()
{
	//If currently using alt attack, use normal attack
	if (bUseAlternateAttack == true)
	{
		bUseAlternateAttack = false;
		return;

	}

	//If currently using normal attack, use alt attack
	if (bUseAlternateAttack == false)
	{
		bUseAlternateAttack = true;
		return;
	}
}

//Called when weapon overlaps something else
void ASkyrimCloneWeapon::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	FHitResult HitData;
	FCollisionQueryParams Params;
	FCollisionShape SphereCollision = FCollisionShape::MakeSphere(12.0f);
	FCollisionObjectQueryParams ObjParams;
	FVector WTraceStart;
	FVector WTraceEnd;
	ObjParams.AddObjectTypesToQuery(ECollisionChannel::ECC_GameTraceChannel1);
}

void ASkyrimCloneWeapon::HandleHeavyAttack()
{
	//If a character is chargin a heavy attack
	if (bIsChargingHeavyAttack)
	{
		//Increment heavy attack timer every frame
		++HeavyAttackTimer;

		//Once heavy attack has been held for 60 frames, release heavy attack and reset all associated variables
		if (HeavyAttackTimer >= 60)
		{
			//Attack before resetting associated variables. The order of operations is important for Attack() to execute as a heavy attack
			Attack();
			bIsChargingHeavyAttack = 0;
			HeavyAttackTimer = 0;
		}
	}
}
