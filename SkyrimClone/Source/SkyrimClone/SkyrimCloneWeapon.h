// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SkyrimCloneWeapon.generated.h"

//Weapon states
UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	WS_Attacking,
	WS_HeavyAttacking,
	WS_Blocking,
	WS_None
};

UENUM(BlueprintType)
enum EFXTypes
{
	FXT_Ricochet,
	FXT_None,
	FXT_HitSword,
	FXT_HitAbsorb,
	FXT_HitWorld,
	FXT_HitFlesh
};

//Weapon types
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	WT_Weapon,
	WT_Shield,
	WT_Spell,
	WT_None
};

// Contains information of a single hitscan weapon linetrace
USTRUCT()
struct FHitTraceInfo
{
	GENERATED_BODY()

public:
	UPROPERTY()
	TEnumAsByte<EFXTypes> FXToPlay;

	UPROPERTY()
	FVector_NetQuantize HitLocation;
};

UCLASS()
class SKYRIMCLONE_API ASkyrimCloneWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASkyrimCloneWeapon();

	//Setup animation montages for weapon
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* LightAttackMontage;

	//Setup animation montages for weapon
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* AlternateAttackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* WeaponEquipMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* WeaponBlockMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* HeavyAttackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	class UAnimMontage* RicochetMontage;

	//Impact particle effects
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class UParticleSystem* ImpactFlesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class UParticleSystem* ImpactWorldFX;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class UParticleSystem* ImpactSwordFX;

	//Impact sounds
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundBase* SwordSwingSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundBase* ImpactFleshSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundBase* BlockStartSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FX)
	class USoundBase* BlockStopSound;

	//Weapon's root component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
	USceneComponent* WeaponRoot;

	//Weapon's mesh component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
	USkeletalMeshComponent* WeaponMesh;

	//Called at the end of the attack animation montage. Stops the tracing process and empties the target list
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void AttackEnd();

	//Called when blocking begins
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void Block();

	//Called when blocking ends
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void BlockEnd();

	//Called when attacking with a magic spell
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void CastSpell();

	bool IsAttacking();

	//Deal damage to an actor
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void DealDamage(APawn* DamageInstigator, AActor* ActorToDmg, float Dmg, FHitResult HitInfo);

	//Play particle effect on hit location
	void PlayImpactEffects(EFXTypes FXToPlay, FVector ImpactLoc);

	//Check to see if an actor has been hurt yet during this attack
	bool CheckHurtCharacters(ACharacter* HurtChar);

	//Holds current state of weapon (Attack, HeavyAttacking, Blocking, None)
	UPROPERTY(BlueprintReadOnly)
	EWeaponState WeaponState = EWeaponState::WS_None;

	//Used to change state of weapon
	void SetWeaponState(EWeaponState NewWeaponState);

	//Whether or not weapon can be swung
	bool CanAttack();

	//Weapon's external name
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	FName Name;

	//Damage type to use when dealing damage
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
	TSubclassOf<UDamageType> DamageType;

	//Damage dealt by light attacks
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
	float LightAttackDamage;

	//Damage dealt by heavy attacks
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
	float HeavyAttackDamage;

	//Amount of traces to perform each swing
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	float TracesPerAttack;

	//Whether or not tracing should be performed
	UPROPERTY(BlueprintReadWrite, Category = Weapon)
	bool bPerformTracing;

	//Toggles tracing on and off
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void ToggleTracing();

	//Use alternate attack animation
	UPROPERTY(BlueprintReadWrite, Category = Weapon)
	bool bUseAlternateAttack = 0;

	//Toggle attack animation to be used
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void ToggleAttackType();

	//Whether or not actor is currently attacking
	UPROPERTY(BlueprintReadOnly)
	bool bCurrentlyAttacking = false;

	//Pointer to last hit actor
	UPROPERTY(BlueprintReadWrite)
	AActor* LastHitActor = nullptr;

	//Overlap handling
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//Sockets on mesh to trace between
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Traces)
	FName SocketStartName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Traces)
	FName SocketEndName;

	//Determines whether a weapon is treated as a weapon, shield, or spell
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	EWeaponType WeaponType;

	//Start point of trace
	FVector TraceStart;

	//Endpoint of trace
	FVector TraceEnd;

	//Endpoint of first socket
	FVector StartSockEnd;

	//Endpoint of second socket
	FVector EndSockEnd;

	//Last frame of sockets during trace
	FVector LastFrameStartSockEnd;
	FVector LastFrameEndSockEnd;

	//OnHit event
	UFUNCTION(BlueprintImplementableEvent, Category = Weapon)
	void OnHit(FHitResult HitInf);

	//Array of characters hit on this swing
	UPROPERTY()
	TArray<ACharacter*> CharactersHitOnThisSwing;

	//Attack with weapon
	void Attack();

	//Cancel charging heavy attack and do a light attack
	void QuickAttack();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FHitTraceInfo HitTraceInfo;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Determines whether or not heavy attack timer increases every frame
	bool bIsChargingHeavyAttack = 0;

private:
	//Used in Tick function to track how many frames a character has been charging a heavy attack, and to release the appopriate attack based on the amount of time held
	void HandleHeavyAttack();

	//Tracks how many frames a heavy attack has been held
	float HeavyAttackTimer = 0;
};
