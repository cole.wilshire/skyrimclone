// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameMenu.h"
#include "Components/Button.h"

#include "../SkyrimCloneGameInstance.h"

bool UInGameMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	//Bind cancel button in main menu Blueprint to trigger CancelPressed
	if (!ensure(CancelButton != nullptr)) return false;
	CancelButton->OnClicked.AddDynamic(this, &UInGameMenu::CancelPressed);

	//Bind cancel button in main menu Blueprint to trigger CancelPressed
	if (!ensure(InventoryButton != nullptr)) return false;
	InventoryButton->OnClicked.AddDynamic(this, &UInGameMenu::InventoryPressed);

	//Bind cancel button in main menu Blueprint to trigger CancelPressed
	if (!ensure(ExitButton != nullptr)) return false;
	ExitButton->OnClicked.AddDynamic(this, &UInGameMenu::ExitPressed);

	return true;
}

//Destroy menu, for use with cancel button
void UInGameMenu::CancelPressed()
{
	Teardown();
}

//Switch to inventory menu
void UInGameMenu::InventoryPressed()
{
	Teardown();

	USkyrimCloneGameInstance* GameInstance = Cast<USkyrimCloneGameInstance>(this->GetGameInstance());
	if (!ensure(GameInstance != nullptr)) return;

	//GameInstance->LoadInventoryMenu();
}

//Return to main menu, for use with exit button
void UInGameMenu::ExitPressed()
{
	//Get world pointer and make sure it's not null
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	//Get first player controller (aka the only player control)
	APlayerController* PlayerController = GEngine->GetFirstLocalPlayerController(World);
	if (!ensure(PlayerController != nullptr)) return;	//Protection from player controller pointer being null causing crash

	//Close interface
	Teardown();

	//Send player to main menu
	PlayerController->ClientTravel("/Game/SkyrimClone/Maps/MainMenu", ETravelType::TRAVEL_Absolute);
}