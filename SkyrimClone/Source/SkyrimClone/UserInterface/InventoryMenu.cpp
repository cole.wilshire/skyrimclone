// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryMenu.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"

#include "../SkyrimCloneGameInstance.h"
#include "../SkyrimCloneWeapon.h"

#include "InventoryRow.h"

UInventoryMenu::UInventoryMenu(const FObjectInitializer& ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> InventoryRowBPClass(TEXT("/Game/SkyrimClone/UserInterface/WBP_InventoryRow"));
	if (!ensure(InventoryRowBPClass.Class != nullptr)) return;

	InventoryRowClass = InventoryRowBPClass.Class;
}

bool UInventoryMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	//Bind exit button to ExitPressed function
	if (!ensure(ExitButton != nullptr)) return false;
	ExitButton->OnClicked.AddDynamic(this, &UInventoryMenu::ExitPressed);

	USkyrimCloneGameInstance* GameInstance = Cast<USkyrimCloneGameInstance>(this->GetGameInstance());
	if (!ensure(GameInstance != nullptr)) return false;

	//Boolean function, so return a boolean
	return true;
}

void UInventoryMenu::Setup(ASkyrimCloneCharacter* InventoryOwner, ASkyrimCloneCharacter* InventoryAccessor)
{
	UMenuWidget::Setup();

	//Save pointer to the character who owns the inventory, for use in other functions
	OwnerCharacter = InventoryOwner;

	//Save pointer to the character who is accessing the inventory, for use in other functions
	AccessingCharacter = InventoryAccessor;

	//Set up item list
	SetInventoryList(InventoryOwner);
}

void UInventoryMenu::SetInventoryList(ASkyrimCloneCharacter* InventoryOwner)
{
	UWorld* World = this->GetWorld();
	if (!ensure(World != nullptr)) return;

	InventoryList->ClearChildren();

	if (InventoryOwner != nullptr)
	{
		for (int i = 0; i < InventoryOwner->Inventory.Num(); ++i)
		{
			//Create item row for use in InventoryList
			UInventoryRow* Row = CreateWidget<UInventoryRow>(World, InventoryRowClass);
			if (!ensure(Row != nullptr)) return;

			//Set item name in row to the name specified weapon
			Row->ItemName->SetText(FText::FromName(InventoryOwner->Inventory[i]->Name));

			//Setup row
			Row->Setup(this, i);

			//Add row to InventoryList
			InventoryList->AddChild(Row);
		}
	}
}

void UInventoryMenu::SelectIndex(uint32 Index)
{
	SelectedIndex = Index;
	
	//If the owner of the inventory is the one accessing it, clicking will equip weapon
	if(OwnerCharacter == AccessingCharacter)
	{
		//Equip weapon at this index
		OwnerCharacter->EquipWeapon(OwnerCharacter->Inventory[Index]);
	}
	//Otherwise, weapons will be looted by the accessor from the owner
	else
	{
		//Give weapon to looter
		AccessingCharacter->TakeWeapon(OwnerCharacter->Inventory[Index], OwnerCharacter);

		//Remove weapon from owner's inventory
		OwnerCharacter->Inventory.RemoveAt(Index);

		//Reset the inventory list to update the loss of the weapon
		SetInventoryList(OwnerCharacter);
	}

	UpdateChildren();
}

//Part of the logic for highlighting selected row
void UInventoryMenu::UpdateChildren()
{
	for (int32 i = 0; i < InventoryList->GetChildrenCount(); ++i)
	{
		UInventoryRow* Row = Cast<UInventoryRow>(InventoryList->GetChildAt(i));
		if (Row != nullptr)
		{
			Row->Selected = (SelectedIndex.IsSet() && SelectedIndex.GetValue() == i);
		}
	}
}

//Close menu when exit button is pressed
void UInventoryMenu::ExitPressed()
{
	Teardown();
}