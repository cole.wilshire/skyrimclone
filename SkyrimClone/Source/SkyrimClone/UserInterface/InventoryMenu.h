// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"

#include "../SkyrimCloneCharacter.h"

#include "InventoryMenu.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API UInventoryMenu : public UMenuWidget
{
	GENERATED_BODY()
	
public:
	UInventoryMenu(const FObjectInitializer& ObjectInitializer);

	void SetInventoryList(ASkyrimCloneCharacter* InventoryOwner);

	void SelectIndex(uint32 Index);

	void Setup(ASkyrimCloneCharacter* InventoryOwner, ASkyrimCloneCharacter* InventoryAccessor);

	ASkyrimCloneCharacter* OwnerCharacter;
	ASkyrimCloneCharacter* AccessingCharacter;

protected:
	virtual bool Initialize() override;

private:
	TSubclassOf<class UUserWidget> InventoryRowClass;

	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* InventoryList;

	UPROPERTY(meta = (BindWidget))
	class UButton* ExitButton;

	TOptional<uint32> SelectedIndex;

	void UpdateChildren();

	UFUNCTION()
	void ExitPressed();
};
