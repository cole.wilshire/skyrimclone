// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryRow.h"
#include "Components/Button.h"

#include "InventoryMenu.h"

void UInventoryRow::Setup(class UInventoryMenu* InParent, uint32 InIndex)
{
	Parent = InParent;
	Index = InIndex;
	RowButton->OnClicked.AddDynamic(this, &UInventoryRow::OnClicked);
}

void UInventoryRow::OnClicked()
{
	Parent->SelectIndex(Index);
}
