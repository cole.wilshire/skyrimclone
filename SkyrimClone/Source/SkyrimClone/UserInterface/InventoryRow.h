// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InventoryRow.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API UInventoryRow : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ItemName;

	UPROPERTY(BlueprintReadOnly)
	bool Selected = false;

	void Setup(class UInventoryMenu* Parent, uint32 Index);

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* RowButton;

	UPROPERTY()
	class UInventoryMenu* Parent;

	uint32 Index;

	UFUNCTION()
	void OnClicked();
};
