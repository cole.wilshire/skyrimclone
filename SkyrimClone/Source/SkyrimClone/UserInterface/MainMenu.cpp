// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenu.h"
#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/Button.h"

bool UMainMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	//Bind play button in main menu Blueprint to ...
	if (!ensure(PlayButton != nullptr)) return false;
	PlayButton->OnClicked.AddDynamic(this, &UMainMenu::PlayPressed);

	//Bind quit button in main menu Blueprint to QuitPressed
	if (!ensure(QuitButton != nullptr)) return false;
	QuitButton->OnClicked.AddDynamic(this, &UMainMenu::QuitPressed);

	//Boolean function, so return a boolean
	return true;
}

//Trigger the game process to end using quit console command
void UMainMenu::PlayPressed()
{
	//Get world pointer and make sure it's not null
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	//Get first player controller (aka the only player control)
	APlayerController* PlayerController = GEngine->GetFirstLocalPlayerController(World);
	if (!ensure(PlayerController != nullptr)) return;	//Protection from player controller pointer being null causing crash

	//Close interface
	Teardown();

	//Send player to main menu
	PlayerController->ClientTravel("/Game/SkyrimClone/Maps/ThirdPersonExampleMap", ETravelType::TRAVEL_Absolute);
}

//Trigger the game process to end using quit console command
void UMainMenu::QuitPressed()
{
	//Get world pointer and make sure it's not null
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	//Get player controller
	APlayerController* PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	//Trigger quit console command, which ends the game process safely
	PlayerController->ConsoleCommand("quit");
}