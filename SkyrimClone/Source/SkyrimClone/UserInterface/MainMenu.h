// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "Components/Button.h"

#include "MainMenu.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API UMainMenu : public UMenuWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;

private:
	//Bind UI buttons to widget. Widget button must be named the same as the buttons in the Blueprint.
	UPROPERTY(meta = (BindWidget))
	class UButton* PlayButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* QuitButton;

	//Begin level
	UFUNCTION()
	void PlayPressed();

	//Quit to desktop
	UFUNCTION()
	void QuitPressed();
};
