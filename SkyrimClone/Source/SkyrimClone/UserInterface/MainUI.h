// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UIWidget.h"
#include "MainUI.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API UMainUI : public UUIWidget
{
	GENERATED_BODY()
	
protected:
	virtual bool Initialize() override;
};
