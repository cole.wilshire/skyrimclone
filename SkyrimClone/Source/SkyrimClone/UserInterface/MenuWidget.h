// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "MenuInterface.h"

#include "MenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	//Setup interface
	void Setup();

	//Close interface
	void Teardown();

	//Create interface
	void SetMenuInterface(IMenuInterface* MenuInterface);

protected:
	//Menu object
	IMenuInterface* MenuInterface;
};
