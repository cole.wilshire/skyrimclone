// Fill out your copyright notice in the Description page of Project Settings.


#include "UIWidget.h"

void UUIWidget::Setup()
{
	//Add widget to viewport over the top of what's already there
	this->AddToViewport();
}

void UUIWidget::SetUIInterface(IUIInterface* Interface)
{
	this->UIInterface = Interface;
}

void UUIWidget::Teardown()
{
	//Remove menu from viewport
	this->RemoveFromViewport();
}