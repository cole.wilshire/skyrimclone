// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "UIInterface.h"

#include "UIWidget.generated.h"

/**
 * 
 */
UCLASS()
class SKYRIMCLONE_API UUIWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	//Setup interface
	void Setup();

	//Close interface
	void Teardown();

	//Create interface
	void SetUIInterface(IUIInterface* UIInterface);

protected:
	//UI Object
	IUIInterface* UIInterface;
};
